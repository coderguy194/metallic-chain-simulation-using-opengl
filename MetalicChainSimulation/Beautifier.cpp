//
//  Beautifier.cpp
//  TexturingLighting
//
//  Created by Md. Abdul Munim Dibosh on 3/8/14.
//  Copyright (c) 2014 Md. Abdul Munim Dibosh. All rights reserved.
//

#include "Beautifier.h"


void drawText(char s[],int textSize,float posX,float posY)
{
    long length = strlen(s);
    glRasterPos2i(posX, posY);
    void * font ;
    switch (textSize) {
        case SMALL_TEXT:
            font = GLUT_BITMAP_HELVETICA_10;
            break;
        case MEDIUM_TEXT:
            font = GLUT_BITMAP_HELVETICA_12;
            break;
        case LARGE_TEXT:
            font = GLUT_BITMAP_HELVETICA_18;
            break;
        default:
            break;
    }
    
    for (int i= 0; i<length; i++)
    {
        char c = s[i];
        glutBitmapCharacter(font, c);
    }
    
}