//
//  ImageHelper.h
//  TexturingLighting
//
//  Created by Md. Abdul Munim Dibosh on 3/7/14.
//  Copyright (c) 2014 Md. Abdul Munim Dibosh. All rights reserved.
//

#ifndef __TexturingLighting__ImageHelper__
#define __TexturingLighting__ImageHelper__

#include <iostream>

#include "imports.h"

#endif /* defined(__TexturingLighting__ImageHelper__) */

GLuint LoadTexture( const char * filename );