//main physics

#include "Physics1.h"									

class Spring                                                
{
public:
	Mass* mass1;										
	Mass* mass2;										

	float springConstant;								
	float springLength;									
	float frictionConstant;								

	Spring(Mass* mass1, Mass* mass2, 
		float springConstant, float springLength, float frictionConstant)		//Constructor
	{
		this->springConstant = springConstant;									
		this->springLength = springLength;										
		this->frictionConstant = frictionConstant;								

		this->mass1 = mass1;													
		this->mass2 = mass2;													
	}
    //we apply forces here
	void solve()																	
	{
		Vector3D springVector = mass1->pos - mass2->pos;							//vector between the two masses
		
		float r = springVector.length();											//distance between the two masses

		Vector3D force;																//force initially has a zero value
		
		if (r != 0)																	//to avoid a division by zero check if r is zero
			force += (springVector / r) * (r - springLength) * (-springConstant);	//the spring force is added to the force

		force += -(mass1->vel - mass2->vel) * frictionConstant;						//the friction force is added to the force
																					//with this addition we obtain the net force of the spring

		mass1->applyForce(force);													//force is applied to mass1
		mass2->applyForce(-force);													//the opposite of force is applied to mass2
	}

};


class RopeSimulation : public Simulation			
{
public:
	Spring** springs;									//Springs binding the masses (there shall be [numOfMasses - 1] of them)

	Vector3D gravitation;								//gravitational acceleration (gravity will be applied to all masses)

	Vector3D ropeConnectionPos;							//A point in space that is used to set the position of the 
														//first mass in the system (mass with index 0)
	
	Vector3D ropeConnectionVel;							//a variable to move the ropeConnectionPos (by this, we can swing the rope)

	float groundRepulsionConstant;						//a constant to represent how much the ground shall repel the masses
	
	float groundFrictionConstant;						//a constant of friction applied to masses by the ground
														//(used for the sliding of rope on the ground)
	
	float groundAbsorptionConstant;						//a constant of absorption friction applied to masses by the ground
														//(used for vertical collisions of the rope with the ground)
	
	float groundHeight;									//a value to represent the y position value of the ground
														//(the ground is a planer surface facing +y direction)

	float airFrictionConstant;							//a constant of air friction applied to masses

	RopeSimulation(										
		int numOfMasses,								//the number of masses
		float m,                                        //weight of each mass in KG
		Vector3D gravitation,                           //value of 'g'
		float groundHeight,								//height of the ground (y position)
		float lastObjectMass                            //the mass of the object at loose end
                   ) : Simulation(numOfMasses, m)					//The super class creates masses with weights m of each
	{
		this->gravitation = gravitation;
        
        
//       
        float springLength = 0.05f;
        float springConstant = 10000.0f;
        float springFrictionConstant = 0.2f;
		
		this->airFrictionConstant = 0.02f;

		this->groundFrictionConstant = 0.2f;
		this->groundRepulsionConstant = 100.0f;
		this->groundAbsorptionConstant = 2.0f;
		this->groundHeight = groundHeight;

        //set mass for the last object at loose end which has the the value of default masses now
        setMassForLastObject(lastObjectMass);
        //initial position setup; we make a horizontally laid rope/chain
		for (int a = 0; a < numOfMasses; ++a)			
		{
			masses[a]->pos.x = a * springLength;		
			masses[a]->pos.y = 0;						
			masses[a]->pos.z = 0;						
		}

		springs = new Spring*[numOfMasses - 1];			//create [numOfMasses - 1] pointers for springs
														//([numOfMasses - 1] springs are necessary for numOfMasses)
		
		for (int a = 0; a < numOfMasses - 1; ++a){
            //spring has 2 mass at 2 ends
			//create the spring with index "a" by the mass with index "a" and another mass with index "a + 1".
			springs[a] = new Spring(masses[a], masses[a + 1], 
				springConstant, springLength, springFrictionConstant);
		}
	}

	void release()										
	{
		Simulation::release();							

		for (int a = 0; a < numOfMasses - 1; ++a)		
		{
			delete(springs[a]);
			springs[a] = NULL;
		}
		
		delete(springs);
		springs = NULL;
	}
    //we always apply forces here
	void solve()										
	{
		for (int a = 0; a < numOfMasses - 1; ++a)		
		{
			springs[a]->solve();						
		}

		for (int a = 0; a < numOfMasses; ++a)				//dibosh check this:start a loop to apply forces which are common for all masses
		{
			masses[a]->applyForce(gravitation * masses[a]->m);				//the gravitational force
			
			masses[a]->applyForce(-masses[a]->vel * airFrictionConstant);	//the air friction;optional

			if (masses[a]->pos.y < groundHeight)		//forces from the ground are applied if a mass collides with the ground
			{
				Vector3D v;								//temp 

				v = masses[a]->vel;						
				v.y = 0;	//ommited to apply sliding effect which is based on x and z component of the velocity:irteza check

				
				masses[a]->applyForce(-v * groundFrictionConstant);		//ground friction force is applied

				v = masses[a]->vel;						//get the velocity
				v.x = 0;								//omit the x and z components of the velocity
				v.z = 0;								//will use v.y in the absorption effect
				
				//above, we obtained a velocity which is vertical to the ground and it will be used in 
				//the absorption force

                //initial ground touching threshold; dibosh edited this;didn't need for now! values except 0.0f doesn't
                //make sense
                float threshold = 0.0f;
				if (v.y < threshold)							//let's absorb energy only when a mass collides towards the ground
					masses[a]->applyForce(-v * groundAbsorptionConstant);		//the absorption force is applied
				
				//the ground shall repel a mass like a spring. 
				//we apply how much displacement is occured and how much force should be applied per unit
                //confusing physics found in web(irteza check wikipedia for explanation again)
				Vector3D repulsiveForce = Vector3D(0, groundRepulsionConstant, 0) * 
					(groundHeight - masses[a]->pos.y);

				masses[a]->applyForce(repulsiveForce);			//ground repulsion force is applied
			}
				
		}


	}

	void simulate(float dt)								//simulate(float dt) is overriden because we want to simulate 
														//the motion of the ropeConnectionPos
	{
		Simulation::simulate(dt);						//super class shall simulate the masses

		ropeConnectionPos += ropeConnectionVel * dt;	//iterate the positon of ropeConnectionPos

        //problematic;object center and graphics simulation center is not same;might seem the object to
        //get inside the ground level sometimes
		if (ropeConnectionPos.y < groundHeight)			//ropeConnectionPos shall not go under the ground
		{
			ropeConnectionPos.y = groundHeight;
			ropeConnectionVel.y = 0;
		}

        //this is the mass where we will show the anchor
		masses[0]->pos = ropeConnectionPos;				//mass with index "0" shall position at ropeConnectionPos
		masses[0]->vel = ropeConnectionVel;				//the mass's velocity is set to be equal to ropeConnectionVel
	}

	void setRopeConnectionVel(Vector3D ropeConnectionVel)	//the method to set ropeConnectionVel
	{
		this->ropeConnectionVel = ropeConnectionVel;
	}
    
    //this method is needed to set the mass for the object at last end
    void setMassForLastObject(float mass){
        masses[numOfMasses-1] = new Mass(mass);
    }

};