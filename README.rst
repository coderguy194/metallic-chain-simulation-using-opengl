Created by `Md. Abdul Munim Dibosh` using *reStructuredText*

=========================================
 Metallic Chain Simulation using OpenGL
=========================================

Introduction
============
It was done as a part of the Computer Graphics course. The purpose was to simulate a real world physics example.

Level : Advanced.

Features
===========
1. Metallic chain formed out of basic shapes.
2. Lighting and Materials applied.

Open Source
===========
This is an Open Source project. If you could add some other interesting stuffs don't forget to shoot a `mail`_. I would love to know.

IF YOU ARE A BEGINNER IN OPENGL CHECK THIS `PROJECT`_.

Demo
====
.. image:: https://bitbucket.org/coderguy194/metallic-chain-simulation-using-opengl/raw/master/metalic.gif

.. GENERAL LINKS

.. _`mail`: abdulmunim.buet@gmail.com
.. _`PROJECT`: https://bitbucket.org/coderguy194/iac-seminar-room-of-buet-by-opengl
